#include <iostream>
#include <signal.h>

#include "src/commands.hpp"
#include "src/mainloop.hpp"
#include "src/timer.hpp"
#include "src/utils.hpp"

using namespace std;

bool _loop = true;

void usage() {
    cout << "RealSense SkelTrack 1.0 " << endl;
    cout << "Usage: " << endl;
    cout << "  ./bin/realsense-skeltrack" << endl;
}

int main(int argc, char *argv[]) {
    signal (SIGINT, keyHandler);

    if ( argc < 3 ) {
	usage();
	return -1;
    }

    int role = GetOption(argv[1]);
    int operation = GetOption(argv[2]);

    if ( operation < 0 && role >= 0 ) {
	cout << "error: Invalid operation '" << argv[2] << "'" << endl;
	usage();
	return -1;
    }

    char *exercise = argv[3];
    int time = GetExerciseTime(argc, argv);

    // Timer
    Timer *timer = new Timer();

    switch (role) {
    case INSTRUCTOR:
	InstructorLoop(operation, exercise, time);
	break;
    case PATIENT:
	PatientLoop(operation, exercise, time);
	break;
    default:
	cout << "error: Invalid role '" << argv[1] << "'" << endl;
	usage();
	break;
    }

    cout << "Execution time: " << timer->TotalTime() << " seconds." << endl;

    return 0;
}
