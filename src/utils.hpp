#include "opencv2/opencv.hpp"

#ifndef UTILS_HPP
#define UTILS_HPP

char* const DATA_PREFIX      = (char *) "./data/";
char* const COLOR_DIR        = (char *) "/color/";
char* const DEPTH_DIR        = (char *) "/depth/";
char* const COLOR_IMG_PREFIX = (char *) "color-";
char* const DEPTH_IMG_PREFIX = (char *) "depth-";
char* const IMG_EXTENTION    = (char *) ".jpg";
char *const TEMPATE2D_FILE   = (char *) "/2d_template.jpg";
char *const DEPTH_FILE       = (char *) "/mean_depth.dat";

char* generateDataPath(char *role, char *exercise);
bool createDirectory(char *path);
bool directoryExists(char *path);
bool fileExists(char *path);
char* getImagePath(char *dataPath, int counter, bool color);
void keyHandler(int s);

cv::Mat get2DTemplate(char *dataPath);
cv::Mat getDepthData(char *dataPath);

#endif
