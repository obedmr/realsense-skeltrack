#include <string.h>

#include <opencv2/opencv.hpp>

#ifndef SKELETON_HPP
#define SKELETON_HPP

using namespace std;

class Skeleton
{
public:
    vector<cv::Point> joints();
    void SaveJointsToDisk(std::string filename);
private:
    vector<cv::Point> _joints;
};

#endif
