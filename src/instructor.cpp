#include <string.h>

#include "opencv2/xfeatures2d.hpp"

#include "camera.hpp"
#include "filecamera.hpp"
#include "realsense.hpp"

#include "cvdefault.hpp"
#include "cvmachine.hpp"

#include "gui.hpp"
#include "opencvgui.hpp"

#include "commands.hpp"
#include "mainloop.hpp"
#include "timer.hpp"
#include "utils.hpp"

using namespace std;

void InstructorLoop(int operation, char *exercise, int time) {

    cout << "instructor: role: " << INSTRUCTOR_CMD << endl;
    cout << "instructor: operation: " << GetOptionName(operation) << endl;

    // RealSense Camera Initialization
    Camera *camera;

    // Initialize Computer Vision machinery
    CVMachine *cvmachine = new CVDefault();

    // Timer
    Timer *timer;

    // GUI
    GUI *gui = new OpenCVGUI(WINDOW_TITLE);
    gui->Init(INPUT_WIDTH, INPUT_HEIGHT);

    char *dataPath = generateDataPath(INSTRUCTOR_CMD, exercise);
    cout << "datapath: " << dataPath << endl;
    int counter = 0;

    switch (operation) {
    case CAPTURE:
	cout << "instructor: capture: " << time << " seconds" <<  endl;

	camera = new RealSense(INPUT_WIDTH, INPUT_HEIGHT, FRAMERATE, dataPath);
	if( !camera->InitializeStreaming() ) {
	    cout << "error: Unable to locate a camera" << endl;
	}

	prepareForCapture(camera, gui, 10);

	timer = new Timer(time);
	cout << "capture: capturing now ... " << endl;
	while ( !timer->Stop() ) {
	    timer->ShowTime();
	    genericCapture(camera, cvmachine, gui, dataPath, counter);
	    counter++;
	}

	camera->Stop();
	cv::destroyAllWindows( );
	break;
    case PROCESS:
	cout << "instructor: process: " << exercise << endl;

	camera = new FileCamera(dataPath);
	if( !camera->InitializeStreaming() ) {
	    cout << "error: Unable to locate a camera" << endl;
	}

	genericProcess(camera, cvmachine, gui);

	camera->Stop();
	cv::destroyAllWindows( );
	break;
    default:
	cout << "error: Invalid option '" << operation << "'" << endl;
	break;
    }
}
