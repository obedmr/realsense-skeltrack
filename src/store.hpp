#include <opencv2/opencv.hpp>

#ifndef STORE_HPP
#define STORE_HPP

using namespace std;

class Store
{
public:
    virtual vector<vector<cv::Point>> ReadJointsPath() = 0;
    virtual bool WriteJoints(vector<cv::Point> joints) = 0;
    virtual bool StartTransaction() = 0;
    virtual bool StopTransaction() = 0;
};

#endif
