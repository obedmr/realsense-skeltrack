#include <iostream>
#include <string.h>

#include "commands.hpp"

using namespace std;

// Helper functions
int GetOption (char *command) {
    int option = -1;

    if (strcmp(command, INSTRUCTOR_CMD) == 0)
	option = INSTRUCTOR;

    if (strcmp(command, CAPTURE_CMD) == 0)
	option = CAPTURE;

    if (strcmp(command, PROCESS_CMD) == 0)
	option = PROCESS;

    if (strcmp(command, PATIENT_CMD) == 0)
	option = PATIENT;

    if (strcmp(command, EVALUATE_CMD) == 0)
	option = EVALUATE;

    if (strcmp(command, HELP_CMD) == 0)
	option = HELP;

    return option;
}

char* GetOptionName(int option) {
    char *optionName = UNSOPPORTED_CMD;

    switch (option) {
    case CAPTURE:
	optionName = CAPTURE_CMD;
	break;
    case PROCESS:
	optionName = PROCESS_CMD;
	break;
    case EVALUATE:
	optionName = EVALUATE_CMD;
	break;
    }

    return optionName;
}

int GetExerciseTime(int argc, char *argv[]) {
    int exerciseTime = 0;

    if (argc >= 5) {
	exerciseTime = atoi(argv[4]);
    }

    return exerciseTime;
}
