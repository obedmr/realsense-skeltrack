#include <opencv2/opencv.hpp>

#include "mainloop.hpp"
#include "algorithms.hpp"

using namespace std;

// Core Algorithms
vector<double> QuadraticPathMatching(vector<vector<cv::Point>> pathsA,
				     vector<vector<cv::Point>> pathsB,
				     int quadraticFactor) {
    vector<double> results(quadraticFactor, 0.0);

    vector<int> patientCuadrants = getCuadrantsStats(pathsA, quadraticFactor);
    vector<int> instructorCuadrants = getCuadrantsStats(pathsB, quadraticFactor);

    for (int i=0; i<results.size(); i++) {
    	if ( instructorCuadrants.at(i) != 0 )
    	    results.at(i) = patientCuadrants.at(i) / instructorCuadrants.at(i);

	if  ( results.at(i) == 0)
	    results.at(i) = 1;

	cout << "----------------------------" << endl;
    	cout << "Cuadrant # " << i << endl;
	cout << "Instructor points : " << instructorCuadrants.at(i) << endl;
	cout << "Patient points    : " << patientCuadrants.at(i) << endl;
	cout <<" Result            : "<< results.at(i) << endl;
    }

    return results;
}

vector<int> getCuadrantsStats(vector<vector<cv::Point>> paths, int quadraticFactor) {
    vector<int> quadrants(quadraticFactor, 0);

    for (int i=0; i<paths.size(); i++) {
	for (int j=0; j<paths.at(i).size(); j++) {
	    quadrants.at(getCuadrant(paths.at(i).at(j), quadraticFactor))++;
	}
    }
    return quadrants;
}

int getCuadrant(cv::Point point, int quadraticFactor) {

     double divFactor = sqrt(quadraticFactor);
     int counter = 0;
     int X = point.x, Y = point.y;

     for (double y = 0; y < INPUT_HEIGHT; y+=INPUT_HEIGHT/divFactor) {
        double yUpLimit = y + INPUT_HEIGHT/divFactor;
        for (double x = 0; x < INPUT_WIDTH; x+=INPUT_WIDTH/divFactor) {
            double xUpLimit = x + INPUT_WIDTH/divFactor;
	    if ( (x <= X) && (X < xUpLimit) && (y <= Y) && (Y < yUpLimit) ) {
	    	return counter;
	    }
	    counter++;
        }
    }

    return --counter;
}


TemplateResult EvaluatePaths(cv::Mat instructor, cv::Mat patient) {

    TemplateResult result{0,0,0,0};
    threshold(patient, patient, 20, 255, 0);
    threshold(instructor, instructor, 20, 255, 0);

    for (int x = 0; x < instructor.rows; x++)
	for (int y = 0; y < instructor.cols; y++) {
	    int instVal = (unsigned short int) instructor.at<uchar>(x,y);
	    int patVal = (unsigned short int) patient.at<uchar>(x,y);
	    if ( instVal > 0 )
		result.instructor++;
	    if (instVal == patVal && instVal != 0)
		result.matched++;
	    if (instVal > 0 && patVal == 0 )
		result.missing++;
	    if (patVal > 0 && instVal == 0 )
		result.extra++;
	}
    return result;
}
DepthResult EvaluateDepth(cv::Mat instructor, cv::Mat patient) {

    DepthResult result{0,0,0};
    float lowThreshold = -40, highThreshold = 40;

    if(const char* env_p = std::getenv("LOW_DEPTH_TRESHOLD")) {
	lowThreshold = atof(env_p);
    }

    if(const char* env_p = std::getenv("HIGH_DEPTH_TRESHOLD")) {
	highThreshold = atof(env_p);
    }

    cv::Mat diff = instructor - patient;
    for (int x = 0; x < diff.rows; x++)
	for (int y = 0; y < diff.cols; y++) {
	    float diffVal = diff.at<float>(x,y);

	    if ( instructor.at<float>(x,y) > 0)
		result.instructor++;

	    if ( lowThreshold <= diffVal &&
		 diffVal <= highThreshold && diffVal != 0) {
		result.matched++;
	    }
	    if ( diffVal > highThreshold )
		result.goForward++;
	    if ( diffVal < lowThreshold )
		result.goBack++;
	}
    return result;
}

cv::Mat cleanDepthData(cv::Mat depthData, cv::Mat baseData) {

    threshold(baseData, baseData, 20, 255, 0);
    for (int x = 0; x < baseData.rows; x++)
	for (int y = 0; y < baseData.cols; y++) {
	    int baseVal = (unsigned short int) baseData.at<uchar>(x,y);
	    if (baseVal == 0) {
		depthData.at<float>(x,y) = 0;
	    }
	}
    return depthData;
}
