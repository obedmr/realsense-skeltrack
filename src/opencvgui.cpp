#include <opencv2/opencv.hpp>
#include <opencv2/highgui.hpp>

#include "gui.hpp"
#include "opencvgui.hpp"

using namespace std;
using namespace cv;

OpenCVGUI::OpenCVGUI(char *windowTitle) {
    this->_windowTitle = windowTitle;
}

bool OpenCVGUI::Init(int width, int height) {
    _width = width;
    _height = height;

    namedWindow( _windowTitle, 0 );
    setMouseCallback( _windowTitle, onMouse, _windowTitle);
    //cvSetWindowProperty( _windowTitle, CV_WND_PROP_FULLSCREEN, CV_WINDOW_FULLSCREEN );

    this->_display = cv::Mat::zeros(Size(_width*2, _height*2), CV_8UC3);

    return true;
}

bool OpenCVGUI::ShowAt(Mat image, int frameType, int panel) {
    Mat subWindow = Mat::zeros(Size(_width, _height), CV_8UC3);

    if(frameType == COLOR_FRAME)
	subWindow = image;
    else
	cvtColor(image, subWindow, COLOR_GRAY2BGR);

    switch(panel) {
    case 1:
	subWindow.copyTo(_display(Rect(0, 0, _width, _height)));
	break;
    case 2:
	subWindow.copyTo(_display(Rect(_width, 0, _width, _height)));
	break;
    case 3:
	subWindow.copyTo(_display(Rect(0, _height, _width, _height)));
	break;
    case 4:
	subWindow.copyTo(_display(Rect(_width, _height, _width, _height)));
	break;
    default:
	cout << "opencvgui : panel out of range"<< endl;
	return false;
	break;
    }

     imshow( _windowTitle, _display );

    return true;
}

void OpenCVGUI::onMouse( int event, int x, int y, int, void* windowName ) {
    if( event == cv::EVENT_LBUTTONDOWN ) {
	cv::destroyAllWindows();
    }
}
