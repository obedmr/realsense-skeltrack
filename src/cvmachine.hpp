#include <opencv2/opencv.hpp>

#include "camera.hpp"

#ifndef CVMACHINE_HPP
#define CVMACHINE_HPP

using namespace std;

class CVMachine
{
public:
    virtual bool MainLoop() = 0;
    virtual cv::Mat SubtractBackground(cv::Mat currentFrame, bool color) = 0;
    virtual vector<cv::Point> DetectJoints(cv::Mat image) = 0;
    virtual vector<cv::KeyPoint> GetFeatures(cv::Mat image) = 0;
};

#endif
