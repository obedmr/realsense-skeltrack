#include "opencv2/opencv.hpp"

#include "camera.hpp"
#include "cvmachine.hpp"
#include "store.hpp"
#include "gui.hpp"

#ifndef MAINLOOP_HPP
#define MAINLOOP_HPP

int const INPUT_WIDTH 	= 640;
int const INPUT_HEIGHT 	= 480;
int const FRAMERATE 	= 60;

char* const WINDOW_TITLE = (char *) "RealSense SkelTrack";
char* const JOINTS_FILE  = (char *) "/joints.bin";

extern bool _loop;

// Generic Functions
void prepareForCapture(Camera *camera, GUI *gui, int time);
void genericCapture(Camera *camera, CVMachine *cvmachine, GUI *gui, char *dataPath, int counter);
void genericProcess(Camera *camera, CVMachine *cvmachine, GUI *gui);

// Instructor functions
void InstructorLoop(int operation, char *exercise, int time);

// Patient functions
void PatientLoop(int operation, char *exercise, int time);
void patientEvaluate(char *exercise, char *instructorPath, char *patientPath);

#endif
