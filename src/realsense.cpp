#include <opencv2/opencv.hpp>
#include <librealsense/rs.hpp>

#include "realsense.hpp"

RealSense::RealSense(int width, int height, int framerate, char *dataPath) {
    this->_width = width;
    this->_height = height;
    this->_framerate = framerate;
    this->_dataPath = dataPath;
}

bool RealSense::InitializeStreaming() {
    if( context.get_device_count() <= 0 )
	return false;

    camera = context.get_device( 0 );
    camera->enable_stream( rs::stream::color, _width, _height, rs::format::rgb8, _framerate );
    camera->enable_stream( rs::stream::depth, _width, _height, rs::format::z16, _framerate );
    camera->start( );

    return true;
}

bool RealSense::VerifyStreaming() {
    if( camera->is_streaming( ) ) {
	camera->wait_for_frames( );
	return true;
    }
    return false;
}

cv::Mat RealSense::GetNextDepthFrame() {
    depth_intrin = camera->get_stream_intrinsics( rs::stream::depth );

    cv::Mat depth( depth_intrin.height,
                     depth_intrin.width,
                     CV_16U,
                     (uint16_t *)camera->get_frame_data( rs::stream::depth ) );

    return depth;
}

cv::Mat RealSense::GetNextColorFrame() {
    color_intrin = camera->get_stream_intrinsics( rs::stream::color );

    cv::Mat rgb( color_intrin.height,
		 color_intrin.width,
		 CV_8UC3,
		 (uchar *)camera->get_frame_data( rs::stream::color ) );

    cv::cvtColor( rgb, rgb, cv::COLOR_BGR2RGB );

    return rgb;
}

cv::Mat RealSense::GetInitialBackground(int type) {
    cv::Mat frame;
    return frame;
}

void RealSense::Stop() {
    camera->stop();
}

double RealSense::GetDepthScale() {
    return camera->get_depth_scale();
}
