#include <iostream>

#include <opencv2/opencv.hpp>

#ifndef CAMERA_HPP
#define CAMERA_HPP

using namespace std;

class Camera
{
public:
    virtual bool InitializeStreaming() = 0;
    virtual bool VerifyStreaming() = 0;
    virtual cv::Mat GetNextDepthFrame() = 0;
    virtual cv::Mat GetNextColorFrame() = 0;
    virtual cv::Mat GetInitialBackground(int type) = 0;
    virtual double GetDepthScale() = 0;
    virtual void Stop() = 0;
    int width();
    int height();
    int framerate();
    char* dataPath();
    bool SaveDepthFrame(string filepath);
    bool SaveColorFrame(string filepath);
protected:
    int _width;
    int _height;
    int _framerate;
    char *_dataPath;
};

#endif
