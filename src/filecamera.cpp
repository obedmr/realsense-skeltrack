#include <string.h>

#include <opencv2/opencv.hpp>

#include "filecamera.hpp"
#include "utils.hpp"

using namespace std;

FileCamera::FileCamera(char *dataPath) {
    this->_dataPath = dataPath;
    this->_increment = 1;
}

FileCamera::FileCamera(char *dataPath, int increment) {
    this->_dataPath = dataPath;
    this->_increment = increment;
}

bool FileCamera::InitializeStreaming() {
    _depthCounter = 0;
    _colorCounter = 0;
    return true;
}

bool FileCamera::VerifyStreaming() {
    if ( _depthCounter < 0 )
	return false;

    _depthCounter+=_increment;
    _colorCounter+=_increment;

     char *filePath = getImagePath(_dataPath, _depthCounter, false);
    if ( !fileExists(filePath))
	return false;

    cout << "filecamera: frame # " << _depthCounter << endl;
    return true;
}

cv::Mat FileCamera::GetNextDepthFrame() {
    char *imagePath = getImagePath(_dataPath, _depthCounter, false);
    cv::Mat depthFrame; //= cv::imread(imagePath, CV_LOAD_IMAGE_GRAYSCALE);
    cv::FileStorage fs(imagePath, cv::FileStorage::READ);
    if( fs.isOpened() == false) {
	cerr<< "No More....Quitting...!";
    }
    fs["depth_frame"] >> depthFrame;
    return depthFrame;
}

cv::Mat FileCamera::GetNextColorFrame() {
    char *imagePath = getImagePath(_dataPath, _colorCounter, true);
    cv::Mat colorFrame = cv::imread(imagePath, CV_LOAD_IMAGE_COLOR);
    return colorFrame;
}

cv::Mat FileCamera::GetInitialBackground(int type) {
    cv::Mat initialFrame;
    return initialFrame;
}

double FileCamera::GetDepthScale() {
    return DEFAULT_SCALE;
}

void FileCamera::Stop() {
    _depthCounter = -1;
    _colorCounter = -1;
}
