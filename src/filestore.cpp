#include <fstream>
#include <string>

#include <opencv2/opencv.hpp>

#include "filestore.hpp"

using namespace std;

FileStore::FileStore(string filename) {
    this->_filename = filename;
}

vector<vector<cv::Point>> FileStore::ReadJointsPath() {

    vector<vector<cv::Point>> jointsPath;

    if ( ! open(INPUT_FILE) ) {
	cout << "Error Reading file" << endl;
    }

    while ( _input.good() ) {
	int size, x, y;
	_input.read(reinterpret_cast<char*>(&size), sizeof(int));
	vector<cv::Point> joints;
	for ( int i = 0; i < size; i++ ) {
	    _input.read(reinterpret_cast<char*>(&x), sizeof(int));
	    _input.read(reinterpret_cast<char*>(&y), sizeof(int));
	    joints.push_back(cv::Point(x,y));
	}
	jointsPath.push_back(joints);
    }

    jointsPath.pop_back();
    close(INPUT_FILE);

    return jointsPath;
}

bool FileStore::WriteJoints(vector<cv::Point> joints) {

    int jointsSize = joints.size();

    if ( _output ) {
	_output.write(reinterpret_cast<char*>(&jointsSize), sizeof(int));

	for ( int i = 0; i < joints.size(); i++ ) {
	    _output.write(reinterpret_cast<char*>(&joints.at(i).x), sizeof(int));
	    _output.write(reinterpret_cast<char*>(&joints.at(i).y), sizeof(int));
	}
    }
    else
	return false;

    return true;
}

bool FileStore::StartTransaction() {
    open(OUTPUT_FILE);
    return true;
}

bool FileStore::StopTransaction() {
    close(OUTPUT_FILE);
    return true;
}

bool FileStore::open(int fileType) {
    if ( fileType == INPUT_FILE )
	_input.open(_filename, ios::in | ios::binary);
    else
	_output.open(_filename, ios::app | ios::binary);

    if ( (!_input && fileType == INPUT_FILE) ||
	 (!_output && fileType == OUTPUT_FILE) )
	return false;

    return true;
}

bool FileStore::close(int fileType) {
    if ( fileType == INPUT_FILE )
	_input.close();
    else
	_output.close();

    return true;
}
