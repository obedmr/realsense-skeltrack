#include <iostream>
#include <time.h>

#include "timer.hpp"

using namespace std;

Timer::Timer(int seconds) {
    this->_seconds = seconds;
    time(&_start);
}

Timer::Timer() {
    time(&_start);
}

bool Timer::Stop() {
    time_t current_time;
    time(&current_time);

    if ( difftime(current_time, _start) >= _seconds )
	return true;

    return false;
}

void Timer::ShowTime() {
    time_t current_time;
    time(&current_time);

    int diff = int(difftime(current_time, _start));
    if (diff != _last ) {
	this->_last = int(diff);
	cout << "Timer: " << diff << " of " << _seconds << " seconds"<< endl;
    }
}

int Timer::TotalTime() {
    time_t current_time;
    time(&current_time);

    return int(difftime(current_time, _start));
}
