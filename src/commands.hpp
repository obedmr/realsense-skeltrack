#ifndef COMMANDS_HPP
#define COMMANDS_HPP

// Command and sub-commands
char* const INSTRUCTOR_CMD  = (char *) "instructor";
char* const PATIENT_CMD     = (char *) "patient";
char* const CAPTURE_CMD     = (char *) "capture";
char* const PROCESS_CMD     = (char *) "process";
char* const EVALUATE_CMD    = (char *) "evaluate";
char* const UNSOPPORTED_CMD = (char *) "unsupported";


// Help command
char* const HELP_CMD = (char *) "help";

// Switch options
const int INSTRUCTOR = 100;
const int PATIENT    = 200;
const int HELP       = 300;
const int CAPTURE    = 1;
const int PROCESS    = 2;
const int EVALUATE   = 3;

// Functions
int GetOption(char *command);
char* GetOptionName(int option);
int GetExerciseTime(int argc, char *argv[]);

#endif
