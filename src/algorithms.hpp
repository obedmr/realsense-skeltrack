#include <opencv2/opencv.hpp>

#ifndef ALGORITHMS_HPP
#define ALGORITHMS_HPP

using namespace std;

struct TemplateResult {
    int instructor;
    int matched;
    int missing;
    int extra;
};

struct DepthResult {
    int instructor;
    int matched;
    int goBack;
    int goForward;
};

// Core Algorithms
vector<double> QuadraticPathMatching(vector<vector<cv::Point>> pathsA,
				     vector<vector<cv::Point>> pathsB,
				     int quadraticFactor);
vector<int> getCuadrantsStats(vector<vector<cv::Point>> paths, int quadraticFactor);
int getCuadrant(cv::Point point, int quadraticFactor);

TemplateResult EvaluatePaths(cv::Mat instructor, cv::Mat patient);
DepthResult EvaluateDepth(cv::Mat instructor, cv::Mat patient);
cv::Mat cleanDepthData(cv::Mat depthData, cv::Mat baseData);

#endif
