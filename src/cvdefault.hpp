#include <opencv2/opencv.hpp>

#include "cvmachine.hpp"

#ifndef CVDEFAULT_HPP
#define CVDEFAULT_HPP

using namespace std;

class CVDefault : public CVMachine
{
public:
    CVDefault();
    bool MainLoop();
    cv::Mat SubtractBackground(cv::Mat currentFrame, bool color);
    vector<cv::Point> DetectJoints(cv::Mat image);
    vector<cv::KeyPoint> GetFeatures(cv::Mat image);

private:
    cv::Ptr<cv::BackgroundSubtractor> _depthBS;
    cv::Ptr<cv::BackgroundSubtractor> _colorBS;

    void init();
};

#endif

