#include <string.h>

#include "skeleton.hpp"

using namespace std;

vector<cv::Point> Skeleton::joints() {
    return _joints;
}

void Skeleton::SaveJointsToDisk(std::string filename) {
    ofstream file (filename, ios::app);
    if (_joints.size() > 0)
 	if (file.is_open()) {
	    int size = sizeof(_joints);
	    file.write (reinterpret_cast<char*>(&_joints), size);
	    file.close();
	}
}
