#include <opencv2/opencv.hpp>

#ifndef GUI_HPP
#define GUI_HPP

const int COLOR_FRAME     = 1;
const int GRAYSCALE_FRAME = 2;

class GUI
{
public:
    virtual bool Init(int width, int height) = 0;
    virtual bool ShowAt(cv::Mat image, int frameType, int panel) = 0;
protected:
    char *_windowTitle;
    int _width;
    int _height;
};

#endif

