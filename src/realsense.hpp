#include <opencv2/opencv.hpp>
#include <librealsense/rs.hpp>

#include "camera.hpp"

#ifndef REALSENSE_HPP
#define REALSENSE_HPP

class RealSense : public Camera
{
public:
    RealSense(int width, int height, int framerate, char *dataPath);
    bool InitializeStreaming();
    bool VerifyStreaming();
    cv::Mat GetNextDepthFrame();
    cv::Mat GetNextColorFrame();
    cv::Mat GetInitialBackground(int type);
    double GetDepthScale();
    void Stop();

private:
    rs::context context;
    rs::device  *camera;
    rs::intrinsics depth_intrin;
    rs::intrinsics color_intrin;
};

#endif
