#include <iostream>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>

#include "opencv2/opencv.hpp"

#include "utils.hpp"

using namespace std;

char* generateDataPath(char *role, char *exercise) {
    char *path = new char[50];

    strcpy(path, DATA_PREFIX);
    if ( !directoryExists(path) )
	createDirectory(path);

    strcat(path, role);
    if ( !directoryExists(path) )
	createDirectory(path);

    strcat(path, "/");
    strcat(path, exercise);
    if ( !directoryExists(path) )
	createDirectory(path);

    char colorPath[50] = "";
    strcat(colorPath, path);
    strcat(colorPath, COLOR_DIR);
    if ( !directoryExists(colorPath) )
	createDirectory(colorPath);

    char depthPath[50] = "";
    strcat(depthPath, path);
    strcat(depthPath, DEPTH_DIR);
    if ( !directoryExists(depthPath) )
    createDirectory(depthPath);

    return path;
}

bool createDirectory(char *path) {
    mode_t nMode = 0755;
    int error = 0;

    cout << "utils: create directory: " << path << endl;
#if defined(_WIN32)
    error = _mkdir(path);
#else
    error = mkdir(path,nMode);
#endif

    if ( error != 0 )
	return false;
    return true;
}

bool directoryExists(char *path) {
    struct stat info;
    cout << "utils: checking directory: " << path << endl;

    if (stat(path, &info) == -1) {
	cout << "utils: directory doesn't exist" << endl;
	return false;
    } else if(info.st_mode & S_IFDIR)
	return true;

    return false;
}

bool fileExists(char *path) {
    struct stat info;

    if (stat(path, &info) == -1) {
	return false;
    } else if(info.st_mode & S_IFMT)
	return true;

    return false;
}


char* getImagePath(char *dataPath, int counter, bool color) {
    char *path = new char[50];
    strcpy(path, dataPath);

    if (color) {
	strcat(path, COLOR_DIR);
	strcat(path, COLOR_IMG_PREFIX);
    } else {
	strcat(path, DEPTH_DIR);
	strcat(path, DEPTH_IMG_PREFIX);
    }

    char intStr[10];
    sprintf(intStr, "%d", counter);
    strcat(path, intStr);
    strcat(path, IMG_EXTENTION);

    return path;
}

void keyHandler(int s) {
    std::cout << "\nCaught signal: " << s << "\n";
    exit(1);
}

cv::Mat get2DTemplate(char *dataPath) {
    cv::Mat templ;
    char *templatePath = new char[50];

    strcpy(templatePath, dataPath);
    strcat(templatePath, TEMPATE2D_FILE);

    cout << "Opening file: " << templatePath << endl;
    templ = cv::imread(templatePath, CV_LOAD_IMAGE_GRAYSCALE);
    return templ;
}
cv::Mat getDepthData(char *dataPath) {
    cv::Mat depth;
    char *depthPath = new char[50];

    strcpy(depthPath, dataPath);
    strcat(depthPath, DEPTH_FILE);

    cout << "Opening file: " << depthPath << endl;
    cv::FileStorage fs(depthPath, cv::FileStorage::READ);
    if( fs.isOpened() == false) {
        cerr<< "No More....Quitting...!";
    }
    fs["depth_mean"] >> depth;

    return depth;
}
