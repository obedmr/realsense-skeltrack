#include <string.h>

#include "opencv2/xfeatures2d.hpp"

#include "camera.hpp"
#include "filecamera.hpp"
#include "realsense.hpp"

#include "cvdefault.hpp"
#include "cvmachine.hpp"

#include "gui.hpp"
#include "opencvgui.hpp"

#include "commands.hpp"
#include "mainloop.hpp"
#include "timer.hpp"
#include "utils.hpp"

#include "algorithms.hpp"

using namespace std;

void PatientLoop(int operation, char *exercise, int time) {

    cout << "role: " << PATIENT_CMD << endl;
    cout << "operation: " << GetOptionName(operation) << endl;

    // RealSense Camera Initialization
    Camera *camera;

    // Initialize Computer Vision machinery
    CVMachine *cvmachine = new CVDefault();

    // Timer
    Timer *timer;

    // GUI
    GUI *gui = new OpenCVGUI(WINDOW_TITLE);
    gui->Init(INPUT_WIDTH, INPUT_HEIGHT);

    char *dataPath = generateDataPath(PATIENT_CMD, exercise);
    cout << "patient    : datapath: " << dataPath << endl;
    char *instructorPath = generateDataPath(INSTRUCTOR_CMD, exercise);
    cout << "instructor : datapath: " << instructorPath << endl;
    int counter = 0;

    switch (operation) {
    case CAPTURE:
	cout << "patient: capture: " << time << " seconds" <<  endl;

	camera = new RealSense(INPUT_WIDTH, INPUT_HEIGHT, FRAMERATE, dataPath);
	if( !camera->InitializeStreaming() ) {
	    cout << "error: Unable to locate a camera" << endl;
	}

	prepareForCapture(camera, gui, 10);

	timer = new Timer(time);

	while ( !timer->Stop() ) {
	    timer->ShowTime();
	    genericCapture(camera, cvmachine, gui, dataPath, counter);
	    counter++;
	}

	camera->Stop();
	cv::destroyAllWindows( );
	break;
    case PROCESS:
	cout << "patient: process: " << exercise << endl;

	camera = new FileCamera(dataPath);
	if( !camera->InitializeStreaming() ) {
	    cout << "error: Unable to locate a camera" << endl;
	}

	// Process Patient data
	genericProcess(camera, cvmachine, gui);

	camera->Stop();
	cv::destroyAllWindows( );
	break;
    case EVALUATE:
	cout << "patient: evaluate: " << exercise << endl;
	patientEvaluate(exercise, instructorPath, dataPath);
	break;
    default:
	cout << "error: Invalid option '" << operation << "'" << endl;
	break;
    }
}

void patientEvaluate(char *exercise, char *instructorPath, char *patientPath) {

    cv::Mat instructor2DTemplate = get2DTemplate(instructorPath);
    cv::Mat patient2DTemplate = get2DTemplate(patientPath);

    cv::Mat instructorDepth = cleanDepthData(getDepthData(instructorPath),
					     instructor2DTemplate);
    cv::Mat patientDepth = cleanDepthData(getDepthData(patientPath),
					  instructor2DTemplate);

    TemplateResult pathResult = EvaluatePaths(instructor2DTemplate, patient2DTemplate);
    DepthResult depthResult = EvaluateDepth(instructorDepth, patientDepth);

    cout << "##################################################" << endl;
    cout << "#"<< endl;
    cout << "# 3D Therapeutic templates matching" << endl;
    cout << "#"<< endl;
    cout << "# Exercise: " << exercise <<  endl;
    cout << "#"<< endl;
    cout << "##################################################" << endl;
    cout << "#"<< endl;
    cout << "# Exercise 2D Paths Match"<< endl;
    cout << "#"<< endl;
    cout << "# Instructor Points : "<< pathResult.instructor << endl;
    cout << "#"<< endl;
    cout << "# Matched Points    : "<< pathResult.matched << endl;
    cout << "# Missing Points    : "<< pathResult.missing << endl;
    cout << "# Extra Points      : "<< pathResult.extra << endl;
    cout << "#"<< endl;
    cout << "##################################################" << endl;
    cout << "#"<< endl;
    cout << "# Depth Data Match with (-20,20) depth threshold"<< endl;
    cout << "#"<< endl;
    cout << "# Instructor Points : "<< depthResult.instructor << endl;
    cout << "#"<< endl;
    cout << "# Matched depth points: "<< depthResult.matched << endl;
    cout << "# Go back points      : "<< depthResult.goBack << endl;
    cout << "# Go forward points   : "<< depthResult.goForward << endl;
    cout << "#"<< endl;
    cout << "##################################################" << endl;
}
