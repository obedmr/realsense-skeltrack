#include <iostream>

#include <opencv2/opencv.hpp>

#include "camera.hpp"

using namespace std;

int Camera::width() {
    return _width;
}

int Camera::height() {
    return _height;
}

int Camera::framerate() {
    return _framerate;
}

char* Camera::dataPath() {
    return _dataPath;
}

bool Camera::SaveDepthFrame(string filepath) {
    //cout << "camera: saving: " << filepath << endl;
    cv::FileStorage fp(filepath, cv::FileStorage::WRITE);
    fp << "depth_frame" << GetNextDepthFrame();
    fp.release();
    return true;
}

bool Camera::SaveColorFrame(string filepath) {
    //cout << "camera: saving: " << filepath << endl;
    return cv::imwrite(filepath, GetNextColorFrame());
}
