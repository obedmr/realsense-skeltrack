#include <opencv2/opencv.hpp>

#include "camera.hpp"

#ifndef FILECAM_HPP
#define FILECAM_HPP

const double DEFAULT_SCALE = 0.001;

class FileCamera : public Camera
{
public:
    FileCamera(char *dataPath);
    FileCamera(char *dataPath, int increment);
    bool InitializeStreaming();
    bool VerifyStreaming();
    cv::Mat GetNextDepthFrame();
    cv::Mat GetNextColorFrame();
    cv::Mat GetInitialBackground(int type);
    double GetDepthScale();
    void Stop();

private:
    int _depthCounter;
    int _colorCounter;
    int _increment;
};

#endif
