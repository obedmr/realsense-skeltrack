#include <opencv2/opencv.hpp>
#include "opencv2/core.hpp"
#include "opencv2/features2d.hpp"
#include "opencv2/xfeatures2d.hpp"
#include "opencv2/highgui.hpp"


#include "cvdefault.hpp"

using namespace std;

CVDefault::CVDefault() {
    init();
}

void CVDefault::init() {
    _depthBS = cv::createBackgroundSubtractorMOG2();
    _colorBS = cv::createBackgroundSubtractorMOG2();
}

bool CVDefault::MainLoop() {
    return true;
}

cv::Mat CVDefault::SubtractBackground(cv::Mat currentFrame, bool color){
    cv::Mat subtract;

    if (color) {
	_colorBS->apply(currentFrame, subtract);
    } else {
        _depthBS->apply(currentFrame, subtract);
    }

    cv::medianBlur(subtract, subtract, 5);
    return subtract;
}

vector<cv::Point> CVDefault::DetectJoints(cv::Mat image) {
    vector<cv::Point> joints;
    vector<cv::KeyPoint> keyPoints = GetFeatures(image);

    for (int i=0; i<keyPoints.size(); i++)
	joints.push_back(keyPoints[i].pt);

    return joints;
}

vector<cv::KeyPoint> CVDefault::GetFeatures(cv::Mat image) {
    std::vector<cv::KeyPoint> features;

    int minHessian = 400;
    cv::Ptr<cv::xfeatures2d::SURF> detector = cv::xfeatures2d::SURF::create( minHessian );
    detector->detect( image, features );

    return features;
}
