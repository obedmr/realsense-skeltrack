#include <time.h>

#include "opencv2/imgproc/imgproc.hpp"

#include "mainloop.hpp"

#include "camera.hpp"
#include "cvmachine.hpp"
#include "gui.hpp"
#include "timer.hpp"
#include "utils.hpp"

using namespace std;

void prepareForCapture(Camera *camera, GUI *gui, int time) {

    Timer *timer = new Timer(time);
    cout << "capture: Prepare for starting the capture in: " << time << " seconds" << endl;

    cv::Mat colorFrame;
    double fontScale = 1.2;
    int thickness = 5;
    cv::Point2f org(100, 100);
    cv::Scalar fontColor(0, 92, 255, 255);
    int fontFace = cv::FONT_HERSHEY_TRIPLEX;

    while ( !timer->Stop() ) {
	timer->ShowTime();
	camera->VerifyStreaming();

	colorFrame = camera->GetNextColorFrame();
	putText(colorFrame, "Prepare for the SHOW", org, fontFace, fontScale, fontColor, thickness);

	cv::Scalar randColor = cv::Scalar(rand() % 255, rand() % 255, rand() % 255);
	cv::Point p1( 320, 240 );
	cv::circle( colorFrame, p1, 20,  randColor, -1, 8, 0 );


	imshow("PREPARE FOR THE SHOW", colorFrame);
	cvWaitKey(1);
    }
    cv::destroyAllWindows( );
}

void genericCapture(Camera *camera, CVMachine *cvmachine, GUI *gui,
		    char *dataPath, int counter) {

    camera->VerifyStreaming();
    camera->SaveDepthFrame(getImagePath(dataPath, counter, false));
    camera->SaveColorFrame(getImagePath(dataPath, counter, true));

    cv::Mat color = camera->GetNextColorFrame();
    cvtColor(color, color, CV_BGR2RGB);

    srand(time(NULL));
    cv::Scalar randColor = cv::Scalar(rand() % 255, rand() % 255, rand() % 255);
    cv::Point p1( 320, 240 );
    cv::circle( color, p1, 20, randColor, -1, 10, 0 );

    gui->ShowAt(color, COLOR_FRAME, 1);
    cvWaitKey(1);
}

void genericProcess(Camera *camera, CVMachine *cvmachine, GUI *gui) {

    cv::Mat depth;
    cv::Mat depthMean;
    cv::Mat color;
    cv::Mat prevGray;
    cv::Mat baseTemplate;
    srand(time(NULL));
    cv::Scalar randColor = cv::Scalar(rand() % 255, rand() % 255, rand() % 255);
    int counter = 0;

    while ( camera->VerifyStreaming() ) {

	depth = camera->GetNextDepthFrame();
	color = camera->GetNextColorFrame();

	// depth data
	if ( depthMean.rows == 0 ) {
	    depthMean = cv::Mat(depth.rows, depth.cols, CV_32F);
	}

	cv::Mat tmpDepth;
	depth.convertTo(tmpDepth, CV_32F);
	cv::accumulate(tmpDepth, depthMean);
	counter++;

	// Grayscale
	cv::Mat gray;
	cvtColor(color, gray, CV_BGR2GRAY );
	blur(gray, gray, cv::Size(9,9));
	medianBlur(gray, gray, 5);

	cv::Mat currentGray;

	if(const char* env_p = std::getenv("LOW_THRESHOLD")) {
	    // Canny Algorithm
	    cv::Mat edges;
	    gray.copyTo(edges);
	    cv::Canny( edges, edges, 15, 45, 3 );
	    edges.copyTo(currentGray);
	} else {
	    gray.copyTo(currentGray);
	}

	// Lucas Kanade / Optical Flow
	int win_size = 15;
	int maxCorners = 20;
	double qualityLevel = 0.05;
	double minDistance = 5.0;
	int blockSize = 3;
	double k = 0.04;
	std::vector<cv::Point2f> cornersA;
	cornersA.reserve(maxCorners);
	std::vector<cv::Point2f> cornersB;
	cornersB.reserve(maxCorners);

	if ( prevGray.rows == 0 ) {
	    currentGray.copyTo(prevGray);
	    baseTemplate = cv::Mat::zeros(color.size(), color.type());
	}
	else {
	    goodFeaturesToTrack( prevGray,cornersA,maxCorners,qualityLevel,
				 minDistance,cv::Mat());
	    goodFeaturesToTrack( currentGray,cornersB,maxCorners,qualityLevel,
				 minDistance,cv::Mat());

	    cornerSubPix( prevGray, cornersA, cv::Size( win_size, win_size ),
			  cv::Size( -1, -1 ),
			  cvTermCriteria( CV_TERMCRIT_ITER | CV_TERMCRIT_EPS,
					  20, 0.03 ) );

	    cornerSubPix( currentGray, cornersB, cv::Size( win_size, win_size ),
			  cv::Size( -1, -1 ),
			  cvTermCriteria( CV_TERMCRIT_ITER | CV_TERMCRIT_EPS,
					  20, 0.03 ) );

	    // Call Lucas Kanade algorithm
	    CvSize pyr_sz = cv::Size( currentGray.size().width+8,
				      currentGray.size().height/3 );

	    std::vector<uchar> features_found;
	    features_found.reserve(maxCorners);
	    std::vector<float> feature_errors;
	    feature_errors.reserve(maxCorners);

	    calcOpticalFlowPyrLK( prevGray, currentGray, cornersA, cornersB,
				  features_found, feature_errors ,
				  cv::Size( win_size, win_size ), 5,
				  cvTermCriteria( CV_TERMCRIT_ITER | CV_TERMCRIT_EPS,
						  20, 0.3 ), 0 );

	    // Make an image of the results
	    for( int i=0; i < features_found.size(); i++ ) {
		cv::Point p0( ceil( cornersA[i].x ), ceil( cornersA[i].y ) );
		cv::Point p1( ceil( cornersB[i].x ), ceil( cornersB[i].y ) );
		cv::circle( baseTemplate, p0, 8,  randColor, -1, 8, 0 );
		cv::circle( baseTemplate, p1, 8,  randColor, -1, 8, 0 );
	    }
	}
	currentGray.copyTo(prevGray);

	gui->ShowAt(color, COLOR_FRAME, 1);
	gui->ShowAt(baseTemplate, COLOR_FRAME, 2);

	int key = cvWaitKey(1);

	if (key > -1)
	    cout << key << endl;
    }

    // Final treatment of template
    blur(baseTemplate, baseTemplate, cv::Size(3,3));
    medianBlur(baseTemplate, baseTemplate, 3);

    erode(baseTemplate, baseTemplate, cv::Mat(), cv::Point(-1, -1), 8);
    dilate(baseTemplate, baseTemplate, cv::Mat(), cv::Point(-1, -1), 2);
    gui->ShowAt(baseTemplate, COLOR_FRAME, 3);

    cv::Mat binaryThreshold =  cv::Mat::zeros(prevGray.size(), prevGray.type());
    cvtColor(baseTemplate, binaryThreshold, CV_BGR2GRAY );
    threshold(binaryThreshold, binaryThreshold, 20, 255, 0);
    gui->ShowAt(binaryThreshold, GRAYSCALE_FRAME, 4);

    // Save 2D template data
    char *templatePath = new char[50];
    strcpy(templatePath, camera->dataPath());
    strcat(templatePath, TEMPATE2D_FILE);
    cout << "process: Saving 2D template at: " << templatePath << endl;
    imwrite(templatePath, binaryThreshold);

    // Save mean depth matrix data
    char *depthPath = new char[50];
    strcpy(depthPath, camera->dataPath());
    strcat(depthPath, DEPTH_FILE);
    cout << "process: Saving Depth data at: " << depthPath << endl;
    cv::FileStorage fp(depthPath, cv::FileStorage::WRITE);
    depthMean /= counter;
    fp << "depth_mean" << depthMean;
    fp.release();


    cout << "[ Ctrl + c ] to finish" << endl;
    cvWaitKey(0);
}
