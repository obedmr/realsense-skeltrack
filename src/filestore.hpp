#include <string>

#include <opencv2/opencv.hpp>

#include "store.hpp"

#ifndef FILESTORE_HPP
#define FILESTORE_HPP

using namespace std;

const int INPUT_FILE  = 1;
const int OUTPUT_FILE = 2;

class FileStore : public Store
{
public:
    FileStore(string dataPath);
    vector<vector<cv::Point>> ReadJointsPath();
    bool WriteJoints(vector<cv::Point> joints);
    bool StartTransaction();
    bool StopTransaction();

private:
    string _filename;
    ifstream _input;
    ofstream _output;
    bool open(int input);
    bool close(int input);
};

#endif
