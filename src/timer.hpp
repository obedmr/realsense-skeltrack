#include <time.h>

#ifndef TIMER_HPP
#define TIMER_HPP

class Timer
{
public:
    Timer();
    Timer(int seconds);
    bool Stop();
    void ShowTime();
    int TotalTime();
private:
    time_t _start;
    double _seconds;
    int _last;
};

#endif
