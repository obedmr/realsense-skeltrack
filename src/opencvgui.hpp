#include <opencv2/opencv.hpp>

#include "gui.hpp"

#ifndef OPENCVGUI_HPP
#define OPENCVGUI_HPP

class OpenCVGUI : public GUI
{
public:
    OpenCVGUI(char *windowTitle);
    bool Init(int width, int height);
    bool ShowAt(cv::Mat image, int frameType, int panel);
private:
    cv::Mat _display;

    static void onMouse( int event, int x, int y, int, void* windowName );
};

#endif

