TODO List
=========

1. Acqusition and Data preparation
   1.1. Depth data
   	- Camera calibration [ON-HOLD] (will check if it's required with the RealSense)
	- Extract 3D point cloud for every frame
	- Perform background substraction using a static pre-recorded background model
	- Perform a 3x3 median filter to reduce noise