# realsense-skeltrack

[license-image]: http://img.shields.io/badge/license-Apache--2-blue.svg?style=flat
[license]: LICENSE

Platform       | Build Status |
-------------- | ------------ |
Linux and OS X |    **OK**    |
Windows        |    **OK**    |


## Dependencies

- cmake
- [librealsense 1.12.1](https://github.com/IntelRealSense/librealsense/tree/v1.12.1) for RealSense Camera support
- [glfw](https://github.com/glfw/glfw) for UI
- [OpenCV](http://opencv.org/)

## Compiling
```
mkdir build
cd build
cmake ..
make
```

## Run it

### Instructor

#### Capture (camera is needed)

```
$ ./bin/realsense-skeltrack instructor capture <name_of_exercise> <time_in_seconds>
```

_Example:_
```
$ ./bin/realsense-skeltrack instructor capture ex1 30
$ tree -L 1 data/instructor/ex1/
data/instructor/ex1/
├── color
└── depth

```

#### Process (camera is not needed)
```
$ ./bin/realsense-skeltrack instructor process <name_of_exercise>
```

_Example:_
```
$ ./bin/realsense-skeltrack instructor process ex1
$ tree -L 1 data/instructor/ex1/
data/instructor/ex1/
├── color
├── depth
└── joints.bin
```

### Patient

#### Capture (camera is needed)
```
$ ./bin/realsense-skeltrack patient capture <name_of_exercise> <time_in_seconds>
```

_Example:_
```
$ ./bin/realsense-skeltrack patient capture ex1 30
$ tree -L 1 data/patient/ex1/
data/patient/ex1/
├── color
└── depth

```

#### Process (camera is not needed)
```
$ ./bin/realsense-skeltrack patient process <name_of_exercise>
```

_Example:_
```
$ ./bin/realsense-skeltrack patient process ex1
$ tree -L 1 data/patient/ex1/
data/patient/ex1/
├── color
├── depth
└── joints.bin
```
