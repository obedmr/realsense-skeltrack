# - Try to find Pango
# Once done this will define
#  PANGO_FOUND - System has pango
#  PANGO_INCLUDE_DIRS - The pango include directories
#  PANGO_LIBRARIES - The libraries needed to use Pango
#  PANGO_DEFINITIONS - Compiler switches required for using Pango

find_package(PkgConfig)
pkg_check_modules(PANGO QUIET pango )
set(PANGO_DEFINITIONS ${PC_PANGO_CFLAGS_OTHER})

find_path(PANGO_INCLUDE_DIR pango-1.0 pango
          HINTS ${PC_PANGO_INCLUDEDIR} ${PC_PANGO_INCLUDE_DIRS}
          PATH_SUFFIXES pango-1.0 pango
	  PATHS /usr/local/include/pango-1.0 )

find_library(PANGO_LIBRARY NAMES pango-1.0 pango
             HINTS ${PC_PANGO_LIBDIR} ${PC_PANGO_LIBRARY_DIRS} )

include(FindPackageHandleStandardArgs)
# handle the QUIETLY and REQUIRED arguments and set PANGO_FOUND to TRUE
# if all listed variables are TRUE
find_package_handle_standard_args(pango DEFAULT_MSG
                                  PANGO_LIBRARY PANGO_INCLUDE_DIR)

mark_as_advanced(PANGO_INCLUDE_DIR PANGO_LIBRARY )

set(PANGO_LIBRARIES ${PANGO_LIBRARY} )
set(PANGO_INCLUDE_DIRS ${PANGO_INCLUDE_DIR} ${PANGO_INCLUDE_DIR}/pango-1.0 )