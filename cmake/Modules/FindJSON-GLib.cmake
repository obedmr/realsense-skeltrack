# - Try to find JSON-GLib
# Once done this will define
#  JSON_GLIB_FOUND - System has json-glib
#  JSON_GLIB_INCLUDE_DIRS - The json-glib include directories
#  JSON_GLIB_LIBRARIES - The libraries needed to use JSON-GLib
#  JSON_GLIB_DEFINITIONS - Compiler switches required for using JSON-GLib

find_package(PkgConfig)
pkg_check_modules(JSON_GLIB QUIET json-glib)
set(JSON_GLIB_DEFINITIONS ${PC_JSON_GLIB_CFLAGS_OTHER})

find_path(JSON_GLIB_INCLUDE_DIR json-glib json-glib-1.0
          HINTS ${PC_JSON_GLIB_INCLUDEDIR} ${PC_JSON_GLIB_INCLUDE_DIRS}
          PATH_SUFFIXES json-glib )

find_library(JSON_GLIB_LIBRARY NAMES json-glib json-glib-1.0
             HINTS ${PC_JSON_GLIB_LIBDIR} ${PC_JSON_GLIB_LIBRARY_DIRS} )

include(FindPackageHandleStandardArgs)
# handle the QUIETLY and REQUIRED arguments and set JSON_GLIB_FOUND to TRUE
# if all listed variables are TRUE
find_package_handle_standard_args(json-glib  DEFAULT_MSG
                                  JSON_GLIB_LIBRARY JSON_GLIB_INCLUDE_DIR)

mark_as_advanced(JSON_GLIB_INCLUDE_DIR JSON_GLIB_LIBRARY )

set(JSON_GLIB_LIBRARIES ${JSON_GLIB_LIBRARY} )
set(JSON_GLIB_INCLUDE_DIRS ${JSON_GLIB_INCLUDE_DIR} ${JSON_GLIB_INCLUDE_DIR}/json-glib-1.0 )