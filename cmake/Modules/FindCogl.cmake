# - Try to find Cogl
# Once done this will define
#  COGL_FOUND - System has cogl
#  COGL_INCLUDE_DIRS - The cogl include directories
#  COGL_LIBRARIES - The libraries needed to use Cogl
#  COGL_DEFINITIONS - Compiler switches required for using Cogl

find_package(PkgConfig)
pkg_check_modules(COGL QUIET cogl cogl-1.0 )
set(COGL_DEFINITIONS ${PC_COGL_CFLAGS_OTHER})

find_path(COGL_INCLUDE_DIR cogl
          HINTS ${PC_COGL_INCLUDEDIR} ${PC_COGL_INCLUDE_DIRS}
          PATH_SUFFIXES cogl)

find_library(COGL_LIBRARY NAMES cogl cogl-1.0
             HINTS ${PC_COGL_LIBDIR} ${PC_COGL_LIBRARY_DIRS} )

include(FindPackageHandleStandardArgs)
# handle the QUIETLY and REQUIRED arguments and set COGL_FOUND to TRUE
# if all listed variables are TRUE
find_package_handle_standard_args(cogl  DEFAULT_MSG
                                  COGL_LIBRARY COGL_INCLUDE_DIR)

mark_as_advanced(COGL_INCLUDE_DIR COGL_LIBRARY )

set(COGL_LIBRARIES ${COGL_LIBRARY} )
set(COGL_INCLUDE_DIRS ${COGL_INCLUDE_DIR} )