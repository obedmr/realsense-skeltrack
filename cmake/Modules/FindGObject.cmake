# - Try to find Gobject
# Once done this will define
#  GOBJECT_FOUND - System has gobject
#  GOBJECT_INCLUDE_DIRS - The gobject include directories
#  GOBJECT_LIBRARIES - The libraries needed to use Gobject
#  GOBJECT_DEFINITIONS - Compiler switches required for using Gobject

find_package(PkgConfig)
pkg_check_modules(GOBJECT QUIET gobject )
set(GOBJECT_DEFINITIONS ${PC_GOBJECT_CFLAGS_OTHER})

find_path(GOBJECT_INCLUDE_DIR gobject-2.0 gobject glib glib-2.0
          HINTS ${PC_GOBJECT_INCLUDEDIR} ${PC_GOBJECT_INCLUDE_DIRS}
          PATH_SUFFIXES gobject-1.0 gobject
	  PATHS /usr/local/include/gobject-2.0 )

find_library(GOBJECT_LIBRARY NAMES gobject-2.0 gobject
             HINTS ${PC_GOBJECT_LIBDIR} ${PC_GOBJECT_LIBRARY_DIRS} )

include(FindPackageHandleStandardArgs)
# handle the QUIETLY and REQUIRED arguments and set GOBJECT_FOUND to TRUE
# if all listed variables are TRUE
find_package_handle_standard_args(gobject DEFAULT_MSG
                                  GOBJECT_LIBRARY GOBJECT_INCLUDE_DIR)

mark_as_advanced(GOBJECT_INCLUDE_DIR GOBJECT_LIBRARY )

set(GOBJECT_LIBRARIES ${GOBJECT_LIBRARY} )
set(GOBJECT_INCLUDE_DIRS ${GOBJECT_INCLUDE_DIR} ${GOBJECT_INCLUDE_DIR}/gobject-2.0 )