# - Try to find Cairo
# Once done this will define
#  CAIRO_FOUND - System has cairo
#  CAIRO_INCLUDE_DIRS - The cairo include directories
#  CAIRO_LIBRARIES - The libraries needed to use Cairo
#  CAIRO_DEFINITIONS - Compiler switches required for using Cairo

find_package(PkgConfig)
pkg_check_modules(CAIRO QUIET cairo)
set(CAIRO_DEFINITIONS ${PC_CAIRO_CFLAGS_OTHER})

find_path(CAIRO_INCLUDE_DIR cairo
          HINTS ${PC_CAIRO_INCLUDEDIR} ${PC_CAIRO_INCLUDE_DIRS}
          PATH_SUFFIXES cairo )

find_library(CAIRO_LIBRARY NAMES cairo
             HINTS ${PC_CAIRO_LIBDIR} ${PC_CAIRO_LIBRARY_DIRS} )

include(FindPackageHandleStandardArgs)
# handle the QUIETLY and REQUIRED arguments and set CAIRO_FOUND to TRUE
# if all listed variables are TRUE
find_package_handle_standard_args(cairo  DEFAULT_MSG
                                  CAIRO_LIBRARY CAIRO_INCLUDE_DIR)

mark_as_advanced(CAIRO_INCLUDE_DIR CAIRO_LIBRARY )

set(CAIRO_LIBRARIES ${CAIRO_LIBRARY} )
set(CAIRO_INCLUDE_DIRS ${CAIRO_INCLUDE_DIR}/cairo )