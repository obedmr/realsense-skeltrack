# - Try to find Skeltrack
# Once done this will define
#  SKELTRACK_FOUND - System has skeltrack
#  SKELTRACK_INCLUDE_DIRS - The skeltrack include directories
#  SKELTRACK_LIBRARIES - The libraries needed to use Skeltrack
#  SKELTRACK_DEFINITIONS - Compiler switches required for using Skeltrack

find_package(PkgConfig)
pkg_check_modules(SKELTRACK QUIET skeltrack-0.1)
set(SKELTRACK_DEFINITIONS ${PC_SKELTRACK_CFLAGS_OTHER})

find_path(SKELTRACK_INCLUDE_DIR skeltrack skeltrack-0.1
          HINTS ${PC_SKELTRACK_INCLUDEDIR} ${PC_SKELTRACK_INCLUDE_DIRS}
          PATH_SUFFIXES skeltrack-0.1 )

find_library(SKELTRACK_LIBRARY NAMES skeltrack skeltrack-0.1
             HINTS ${PC_SKELTRACK_LIBDIR} ${PC_SKELTRACK_LIBRARY_DIRS} )

include(FindPackageHandleStandardArgs)
# handle the QUIETLY and REQUIRED arguments and set SKELTRACK_FOUND to TRUE
# if all listed variables are TRUE
find_package_handle_standard_args(skeltrack  DEFAULT_MSG
                                  SKELTRACK_LIBRARY SKELTRACK_INCLUDE_DIR)

mark_as_advanced(SKELTRACK_INCLUDE_DIR SKELTRACK_LIBRARY )

set(SKELTRACK_LIBRARIES ${SKELTRACK_LIBRARY} )
set(SKELTRACK_INCLUDE_DIRS ${SKELTRACK_INCLUDE_DIR}/skeltrack-0.1 )