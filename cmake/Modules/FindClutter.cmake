# - Try to find Clutter
# Once done this will define
#  CLUTTER_FOUND - System has clutter
#  CLUTTER_INCLUDE_DIRS - The clutter include directories
#  CLUTTER_LIBRARIES - The libraries needed to use Clutter
#  CLUTTER_DEFINITIONS - Compiler switches required for using Clutter

find_package(PkgConfig)
pkg_check_modules(CLUTTER QUIET clutter-1.0)
set(CLUTTER_DEFINITIONS ${PC_CLUTTER_CFLAGS_OTHER})

find_path(CLUTTER_INCLUDE_DIR clutter clutter-1.0
          HINTS ${PC_CLUTTER_INCLUDEDIR} ${PC_CLUTTER_INCLUDE_DIRS}
          PATH_SUFFIXES clutter-1.0 clutter )

find_library(CLUTTER_LIBRARY NAMES clutter clutter-1.0
             HINTS ${PC_CLUTTER_LIBDIR} ${PC_CLUTTER_LIBRARY_DIRS} )

include(FindPackageHandleStandardArgs)
# handle the QUIETLY and REQUIRED arguments and set CLUTTER_FOUND to TRUE
# if all listed variables are TRUE
find_package_handle_standard_args(clutter DEFAULT_MSG
                                  CLUTTER_LIBRARY CLUTTER_INCLUDE_DIR)

mark_as_advanced(CLUTTER_INCLUDE_DIR CLUTTER_LIBRARY )

set(CLUTTER_LIBRARIES ${CLUTTER_LIBRARY} )
set(CLUTTER_INCLUDE_DIRS ${CLUTTER_INCLUDE_DIR} )