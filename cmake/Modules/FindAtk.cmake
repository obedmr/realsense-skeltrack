# - Try to find Atk
# Once done this will define
#  ATK_FOUND - System has atk
#  ATK_INCLUDE_DIRS - The atk include directories
#  ATK_LIBRARIES - The libraries needed to use Atk
#  ATK_DEFINITIONS - Compiler switches required for using Atk

find_package(PkgConfig)
pkg_check_modules(ATK QUIET atk)
set(ATK_DEFINITIONS ${PC_ATK_CFLAGS_OTHER})

find_path(ATK_INCLUDE_DIR atk atk-1.0
          HINTS ${PC_ATK_INCLUDEDIR} ${PC_ATK_INCLUDE_DIRS}
          PATH_SUFFIXES atk )

find_library(ATK_LIBRARY NAMES atk atk-1.0
             HINTS ${PC_ATK_LIBDIR} ${PC_ATK_LIBRARY_DIRS} )

include(FindPackageHandleStandardArgs)
# handle the QUIETLY and REQUIRED arguments and set ATK_FOUND to TRUE
# if all listed variables are TRUE
find_package_handle_standard_args(atk  DEFAULT_MSG
                                  ATK_LIBRARY ATK_INCLUDE_DIR)

mark_as_advanced(ATK_INCLUDE_DIR ATK_LIBRARY )

set(ATK_LIBRARIES ${ATK_LIBRARY} )
set(ATK_INCLUDE_DIRS ${ATK_INCLUDE_DIR} ${ATK_INCLUDE_DIR}/atk-1.0 )