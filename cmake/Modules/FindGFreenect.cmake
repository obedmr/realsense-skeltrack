# - Try to find Gfreenect
# Once done this will define
#  GFREENECT_FOUND - System has gfreenect
#  GFREENECT_INCLUDE_DIRS - The gfreenect include directories
#  GFREENECT_LIBRARIES - The libraries needed to use Gfreenect
#  GFREENECT_DEFINITIONS - Compiler switches required for using Gfreenect

find_package(PkgConfig)
pkg_check_modules(GFREENECT QUIET gfreenect-0.1)
set(GFREENECT_DEFINITIONS ${PC_GFREENECT_CFLAGS_OTHER})

find_path(GFREENECT_INCLUDE_DIR gfreenect gfreenect-0.1
          HINTS ${PC_GFREENECT_INCLUDEDIR} ${PC_GFREENECT_INCLUDE_DIRS}
          PATH_SUFFIXES gfreenect-0.1 )

find_library(GFREENECT_LIBRARY NAMES gfreenect gfreenect-0.1
             HINTS ${PC_GFREENECT_LIBDIR} ${PC_GFREENECT_LIBRARY_DIRS} )

include(FindPackageHandleStandardArgs)
# handle the QUIETLY and REQUIRED arguments and set GFREENECT_FOUND to TRUE
# if all listed variables are TRUE
find_package_handle_standard_args(gfreenect  DEFAULT_MSG
                                  GFREENECT_LIBRARY GFREENECT_INCLUDE_DIR)

mark_as_advanced(GFREENECT_INCLUDE_DIR GFREENECT_LIBRARY )

set(GFREENECT_LIBRARIES ${GFREENECT_LIBRARY} )
set(GFREENECT_INCLUDE_DIRS ${GFREENECT_INCLUDE_DIR}/gfreenect-0.1 )